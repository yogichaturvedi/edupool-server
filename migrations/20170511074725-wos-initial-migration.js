'use strict';
const prepareTitle = require("../src/utils").prepareTitle;

let async = require('async');
let _ = require('lodash');
const Sequelize = require('sequelize');
const uuidV4 = require('uuid/v4');
const users = require('../master-data/user.json');
let dbm;
let type;
let seed;

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function (options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

function createTables(db) {
  let promises = [];
  users.forEach(function (data) {
    console.log(Sequelize.UUIDV4);
    promises.push(db.insert.bind(db, 'user', ['id', 'username', 'password', 'email', 'mobile_number'], [uuidV4(), data.username, data.password, data.email, data.mobileNumber]));
  });

  return [
    createUserTable(db),
    createCoachingTable(db),
    createCollegeTable(db),
    createEventTable(db),
    createExamTable(db),
    createResultTable(db),
    createSchoolTable(db),
    createStudyTable(db),
    createHomeInfoTable(db),
    createRatingReviewTable(db),
    createBlogTable(db),
  ].concat(promises);
}

function changePrimaryKeyColumn(db, callback) {
  let tables = ['school', 'college', 'coaching', 'exam', 'event', 'result'];

  _.forEach(tables, (tableName) => {
    let alterCommand = "ALTER TABLE " + tableName + " ALTER COLUMN id SET DATA TYPE VARCHAR";
    console.log("Running - " + alterCommand);

    db.runSql(alterCommand, []);
    db.runSql(`SELECT id, basic_details FROM ${tableName}`, [], callback).then((data) => {
      _.forEach(data.rows, (row, index) => {
        const isTitleDuplicate = _.some(data.rows, (r) => {
          return (prepareTitle(r.basic_details.title) === prepareTitle(row.basic_details.title)) && r.id !== row.id;
        });
        if (isTitleDuplicate) {
          console.log("Duplicate Key found ----- " + row.basic_details.title);
        }
        const title = isTitleDuplicate ? (`${row.basic_details.title}-${index}`) : row.basic_details.title;
        let command = `UPDATE ${tableName} SET id='${prepareTitle(title)}' where id='${row.id}'`;
        console.log(command);
        db.runSql(command, []);
      });
    });
  });


  _.forEach(['study', 'blog'], (tableName) => {
    let alterCommand = "ALTER TABLE " + tableName + " ALTER COLUMN id SET DATA TYPE VARCHAR";
    console.log("Running - " + alterCommand);
    db.runSql(alterCommand, []);
    db.runSql(`SELECT id, title FROM ${tableName}`, [], callback).then((data) => {
      _.forEach(data.rows, (row, index) => {
        const isTitleDuplicate = _.some(data.rows, (r) => {
          return (prepareTitle(r.title) === prepareTitle(row.title)) && r.id !== row.id;
        });
        if (isTitleDuplicate) {
          console.log("Duplicate Key found ----- " + row.title);
        }
        const title = isTitleDuplicate ? (`${row.title}-${index}`) : row.title;
        let command = `UPDATE ${tableName} SET id='${prepareTitle(title)}' where id='${row.id}'`;
        console.log(command);
        db.runSql(command, []);
      });
    });
  });
}


exports.up = function (db, callback) {
  // const createTablePromises = createTables(db);
  // changePrimaryKeyColumn(db, callback);
  async.series(createTables(db), callback);
};

exports.down = function (db, callback) {
};

exports._meta = {
  "version": 4
};

function createUserTable(db) {
  return db.createTable.bind(db, 'user', {
    columns: {
      id: {type: 'UUID', primaryKey: true},
      created_on: {type: 'timestamp with time zone'},
      updated_on: {type: 'timestamp with time zone'},
      created_by: {type: 'string', length: 255},
      username: {type: 'string', length: 255},
      email: {type: 'string', length: 255},
      password: {type: 'string', length: 255},
      profile_photo: {type: 'string', length: 255},
      mobile_number: {type: 'string', length: 255},
    },
    ifNotExists: true
  });
}

function createCoachingTable(db) {
  return db.createTable.bind(db, 'coaching', {
    columns: {
      id: {type: 'string', primaryKey: true},
      created_on: {type: 'timestamp with time zone'},
      updated_on: {type: 'timestamp with time zone'},
      created_by: {type: 'string', length: 255},
      basic_details: {type: 'json'},
      gallery: {type: 'json'},
      about_us: {type: 'json'},
      course: {type: 'json'},
      hostel_and_buses: {type: 'json'},
      contact_us: {type: 'json'},
      events: {type: 'json'},
      social_links: {type: 'json'},
      rating: {type: 'json'},
      tags: {type: 'json'},
      reviewed: {type: 'boolean'},
      featured: {type: 'boolean'},
      admission_open: {type: 'boolean'},
      audit_info: {type: 'json'}
    },
    ifNotExists: true
  });
}

function createCollegeTable(db) {
  return db.createTable.bind(db, 'college', {
    columns: {
      id: {type: 'string', primaryKey: true},
      created_on: {type: 'timestamp with time zone'},
      updated_on: {type: 'timestamp with time zone'},
      created_by: {type: 'string', length: 255},
      basic_details: {type: 'json'},
      gallery: {type: 'json'},
      about_us: {type: 'json'},
      course: {type: 'json'},
      hostel_and_buses: {type: 'json'},
      contact_us: {type: 'json'},
      events: {type: 'json'},
      social_links: {type: 'json'},
      rating: {type: 'json'},
      tags: {type: 'json'},
      reviewed: {type: 'boolean'},
      featured: {type: 'boolean'},
      admission_open: {type: 'boolean'},
      audit_info: {type: 'json'}
    },
    ifNotExists: true
  });
}

function createEventTable(db) {
  return db.createTable.bind(db, 'event', {
    columns: {
      id: {type: 'string', primaryKey: true},
      created_on: {type: 'timestamp with time zone'},
      updated_on: {type: 'timestamp with time zone'},
      created_by: {type: 'string', length: 255},
      basic_details: {type: 'json'},
      activities: {type: 'json'},
      cover_image: {type: 'string'},
      faqs: {type: 'json'},
      terms_and_conditions: {type: 'json'},
      social_links: {type: 'json'},
      tags: {type: 'json'}
    },
    ifNotExists: true
  });
}

function createExamTable(db) {
  return db.createTable.bind(db, 'exam', {
    columns: {
      id: {type: 'string', primaryKey: true},
      created_on: {type: 'timestamp with time zone'},
      updated_on: {type: 'timestamp with time zone'},
      created_by: {type: 'string', length: 255},
      basic_details: {type: 'json'},
      notice: {type: 'json'},
      number_of_vacancies: {type: 'string'},
      criteria: {type: 'json'},
      important_dates: {type: 'json'},
      fees: {type: 'string'},
      selection_procedure: {type: 'string'},
      how_to_apply: {type: 'json'},
      official_website: {type: 'json'},
      tags: {type: 'json'},
    },
    ifNotExists: true
  });
}

function createResultTable(db) {
  return db.createTable.bind(db, 'result', {
    columns: {
      id: {type: 'string', primaryKey: true},
      created_on: {type: 'timestamp with time zone'},
      updated_on: {type: 'timestamp with time zone'},
      created_by: {type: 'string', length: 255},
      basic_details: {type: 'json'},
      tags: {type: 'json'}
    },
    ifNotExists: true
  });
}

function createSchoolTable(db) {
  return db.createTable.bind(db, 'school', {
    columns: {
      id: {type: 'string', primaryKey: true},
      created_on: {type: 'timestamp with time zone'},
      updated_on: {type: 'timestamp with time zone'},
      created_by: {type: 'string', length: 255},
      basic_details: {type: 'json'},
      gallery: {type: 'json'},
      about_us: {type: 'json'},
      course: {type: 'json'},
      hostel_and_buses: {type: 'json'},
      contact_us: {type: 'json'},
      events: {type: 'json'},
      social_links: {type: 'json'},
      rating: {type: 'json'},
      tags: {type: 'json'},
      reviewed: {type: 'boolean'},
      featured: {type: 'boolean'},
      admission_open: {type: 'boolean'},
      audit_info: {type: 'json'}
    },
    ifNotExists: true
  });
}

function createStudyTable(db) {
  return db.createTable.bind(db, 'study', {
    columns: {
      id: {type: 'string', primaryKey: true},
      created_on: {type: 'timestamp with time zone'},
      updated_on: {type: 'timestamp with time zone'},
      created_by: {type: 'string', length: 255},
      title: {type: 'string'},
      description: {type: 'string'},
      links: {type: 'json'},
      tags: {type: 'json'},
    },
    ifNotExists: true
  });
}

function createHomeInfoTable(db) {
  return db.createTable.bind(db, 'home-info', {
    columns: {
      id: {type: 'UUID', primaryKey: true},
      created_on: {type: 'timestamp with time zone'},
      updated_on: {type: 'timestamp with time zone'},
      created_by: {type: 'string', length: 255},
      about_us: {type: 'string'},
      info_email: {type: 'string'},
      query_email: {type: 'string'},
      description: {type: 'string'},
      contact_numbers: {type: 'json'},
      social_links: {type: 'json'},
      address: {type: 'string'},
      images: {type: 'json'},
      tags: {type: 'json'},
    },
    ifNotExists: true
  });
}

function createRatingReviewTable(db) {
  return db.createTable.bind(db, 'rating-review', {
    columns: {
      id: {type: 'UUID', primaryKey: true},
      created_on: {type: 'timestamp with time zone'},
      updated_on: {type: 'timestamp with time zone'},
      created_by: {type: 'string', length: 255},
      institute_id: {type: 'string'},
      institute_type: {type: 'string'},
      value: {type: 'integer'},
      user: {type: 'json'},
      message: {type: 'string'}
    },
    ifNotExists: true
  });
}

function createBlogTable(db) {
  return db.createTable.bind(db, 'blog', {
    columns: {
      id: {type: 'string', primaryKey: true},
      created_on: {type: 'timestamp with time zone'},
      updated_on: {type: 'timestamp with time zone'},
      created_by: {type: 'string', length: 255},
      title: {type: 'string'},
      type: {type: 'string'},
      short_description: {type: 'string'},
      category: {type: 'string'},
      thumbnail: {type: 'string'},
      content: {type: 'string'},
      comment: {type: 'json'},
      show: {type: 'boolean'},
      tags: {type: 'json'},
    },
    ifNotExists: true
  });
}
