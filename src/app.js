'use strict';

const path = require('path');
const serveStatic = require('feathers').static;
const favicon = require('serve-favicon');
const compress = require('compression');
const feathers = require('feathers');
const configuration = require('feathers-configuration');
const hooks = require('feathers-hooks');
const rest = require('feathers-rest');
const bodyParser = require('body-parser');
const socketio = require('feathers-socketio');
const middleware = require('./middleware');
const services = require('./services');
const app = feathers();
app.configure(configuration(path.join(__dirname, '..')));
const Sequelize = require('sequelize');
var errorHandler = require('feathers-errors/handler');
var auth = require('feathers-authentication');
const jwt = require('feathers-authentication-jwt');
const databaseConfig = require('../config/database.json')[app.get('db-config')];
const memory = require('feathers-memory');
var cors = require('cors');

app.connection = new Sequelize(databaseConfig.database, databaseConfig.user, databaseConfig.password, {
  host: databaseConfig.host,
  port: databaseConfig.port,
  dialect: databaseConfig.dialect
});

function restFormatter(req, res) {
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Request-Method', '*');
	res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET');
	res.setHeader('Access-Control-Allow-Headers', '*');
  if (req.method === 'POST' && res.data && res.data.id) {
    res.location(req.path + "/" + res.data.id);
    res.end();
  }
  if (res.data && res.data.data) {
    res.json(res.data.data);
  }
  else {
    res.json(res.data);
  }
}

app.use(compress())
  .use(favicon(path.join(app.get('public'), 'favicon.ico')))
  .use('/', serveStatic(app.get('public')))
  .use(bodyParser.json({limit: '50mb'}))
  .use(bodyParser.urlencoded({extended: true,limit: '50mb'}))
  .use('/users',memory())
  .use(cors())
  .configure(hooks())
  .configure(rest(restFormatter))
  .configure(socketio())
  .configure(auth({secret: 'super secret'}))
  .configure(jwt())
  .configure(services)
  .configure(middleware);


module.exports = app;
