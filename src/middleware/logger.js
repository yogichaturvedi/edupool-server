'use strict';

const winston = require('winston');

module.exports = function (app) {
  app.logger = winston;
  winston.add(winston.transports.File, {
    filename: 'log/edupool-server.log',
    handleExceptions: true,
    humanReadableUnhandledException: true
  })
    .remove(winston.transports.Console);
  return function (error, req, res, next) {
    if (error) {
      const message = `${error.code ? `(${error.code}) ` : '' }Route: ${req.url} - ${error.message}`;

      if (error.code === 404) {
        winston.info(message);
      }
      else {
        winston.error(message);
        winston.info(error.stack);
      }
    }

    next(error);
  };
};
