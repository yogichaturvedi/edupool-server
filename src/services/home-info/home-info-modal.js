'use strict';

const Sequelize = require('sequelize');
module.exports = function (app) {
  return app.connection.define('home-info', {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        unique: true,
        isUUID: 4,
        defaultValue: Sequelize.UUIDV4
      },
      createdBy: {
        field: "created_by",
        type: Sequelize.STRING
      },
      createdAt: {
        field: "created_on",
        type: Sequelize.DATE
      },
      updatedAt: {
        field: "updated_on",
        type: Sequelize.DATE
      },
      aboutUs: {
        field: "about_us",
        type: Sequelize.STRING
      },
      infoEmail: {
        field: "info_email",
        type: Sequelize.STRING
      },
      queryEmail: {
        field: "query_email",
        type: Sequelize.STRING
      },
      contactNumbers: {
        field: "contact_numbers",
        type: Sequelize.JSON
      },
      socialLinks: {
        field: "social_links",
        type: Sequelize.JSON
      },
      address: {
        field: "address",
        type: Sequelize.STRING
      },
      images: {
        field: "images",
        type: Sequelize.JSON
      }
    },
    {
      tableName: 'home-info'
    });
};
