'use strict';

const service = require('feathers-sequelize');
const exam = require('./home-info-modal');
const hooks = require('./hooks');

module.exports = function() {
  const app = this;

  const options = {
    Model: exam(app),
    paginate: {
    }
  };

  // Initialize our service with any options it requires
  app.use('/edu-pool-server/v1/home-info', service(options));

  // Get our initialize service to that we can bind hooks
  const examService = app.service('/edu-pool-server/v1/home-info');

  // Set up our before hooks
  examService.before(hooks.before);

  // Set up our after hooks
  examService.after(hooks.after);
};
