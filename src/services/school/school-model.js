'use strict';

const Sequelize = require('sequelize');
module.exports = function (app) {
  return app.connection.define('school', {
      id: {
        type: Sequelize.STRING,
        primaryKey: true,
        unique: true
      },
      createdBy: {
        field: "created_by",
        type: Sequelize.STRING
      },
      createdAt: {
        field: "created_on",
        type: Sequelize.DATE
      },
      updatedAt: {
        field: "updated_on",
        type: Sequelize.DATE
      }, basicDetails: {
        field: "basic_details",
        type: Sequelize.JSON
      },
      gallery: {
        type: Sequelize.JSON
      },
      aboutUs: {
        field: "about_us",
        type: Sequelize.JSON
      },
      course: {
        type: Sequelize.JSON
      },
      hostelAndBuses: {
        field: "hostel_and_buses",
        type: Sequelize.JSON
      },
      contactUs: {
        field: "contact_us",
        type: Sequelize.JSON
      },
      events: {
        type: Sequelize.JSON
      },
      socialLinks: {
        field: "social_links",
        type: Sequelize.JSON
      },
      rating: {
        type: Sequelize.JSON
      },
      tags: {
        type: Sequelize.JSON
      },
      reviewed: {
        type: Sequelize.BOOLEAN,
        defaultValue:false
      },
      featured: {
        type: Sequelize.BOOLEAN,
        defaultValue:false
      },
      admissionOpen: {
        field: "admission_open",
        type: Sequelize.BOOLEAN,
        defaultValue:false
      },
      auditInfo: {
        field:"audit_info",
        type: Sequelize.JSON
      }
    },
    {
      tableName: 'school'
    });
};
