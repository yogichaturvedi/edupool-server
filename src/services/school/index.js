'use strict';

const service = require('feathers-sequelize');
const school = require('./school-model');
const hooks = require('./hooks');
const _ = require("lodash");
const Fuse = require("fuse.js");

module.exports = function() {
  const app = this;

  const options = {
    Model: school(app),
    paginate: {


    }
  };

  // Initialize our service with any options it requires
  app.use('/edu-pool-server/v1/school', service(options));

  // Get our initialize service to that we can bind hooks
  const schoolService = app.service('/edu-pool-server/v1/school');

  // Set up our before hooks
  schoolService.before(hooks.before);

  // Set up our after hooks
  schoolService.after(hooks.after);

  //Search in school
  app.use('/edu-pool-server/v1/search/school', {
    find(request) {
      let keywords = request.query.keywords;
      return schoolService.find().then((dataList) => {
        let options = {
          shouldSort: true,
          threshold: 0.3,
          location: 0,
          distance: 100,
          maxPatternLength: 32,
          minMatchCharLength: 3,
          keys: ['tags', 'basicDetails.title']
        };
        let fuse = new Fuse(dataList, options);
        return fuse.search(keywords);
      });
    }
  });
};
