'use strict';
const _ = require("lodash");
const service = require('feathers-sequelize');
const coaching = require('./coaching-model');
const hooks = require('./hooks');
const Fuse = require("fuse.js");

module.exports = function () {
  const app = this;

  const options = {
    Model: coaching(app),
    paginate: {}
  };

  // Initialize our service with any options it requires
  app.use('/edu-pool-server/v1/coaching', service(options));

  // Get our initialize service to that we can bind hooks
  const coachingService = app.service('/edu-pool-server/v1/coaching');

  // Set up our before hooks
  coachingService.before(hooks.before);

  // Set up our after hooks
  coachingService.after(hooks.after);

  //Search in coaching
  app.use('/edu-pool-server/v1/search/coaching', {
    find(request) {
      let keywords = request.query.keywords;
      return coachingService.find().then((dataList) => {
        let options = {
          shouldSort: true,
          threshold: 0.3,
          location: 0,
          distance: 100,
          maxPatternLength: 32,
          minMatchCharLength: 3,
          keys: ['tags', 'basicDetails.title']
        };
        let fuse = new Fuse(dataList, options);
        return fuse.search(keywords);
      });
    }
  });

  //Search in coaching
  app.use('/edu-pool-server/v1/mail', {
    create(request,body) {
      console.log(request);
      console.log(body);

      return request;
      // let keywords = request.query.keywords;
      // return coachingService.find().then((dataList) => {
      //   let options = {
      //     shouldSort: true,
      //     threshold: 0.3,
      //     location: 0,
      //     distance: 100,
      //     maxPatternLength: 32,
      //     minMatchCharLength: 3,
      //     keys: ['tags', 'basicDetails.title']
      //   };
      //   let fuse = new Fuse(dataList, options);
      //   return fuse.search(keywords);
      // });
    }
  });
};
