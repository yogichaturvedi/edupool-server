'use strict';

const service = require('feathers-sequelize');
const blog = require('./blog-model');
const hooks = require('./hooks');
const _ = require("lodash");
const Fuse = require("fuse.js");

module.exports = function() {
  const app = this;

  const options = {
    Model: blog(app),
    paginate: {
    }
  };

  // Initialize our service with any options it requires
  app.use('/edu-pool-server/v1/blog', service(options));

  // Get our initialize service to that we can bind hooks
  const blogService = app.service('/edu-pool-server/v1/blog');

  // Set up our before hooks
  blogService.before(hooks.before);

  // Set up our after hooks
  blogService.after(hooks.after);

  //Search in blog
  app.use('/edu-pool-server/v1/search/blog', {
    find(request) {
      let keywords = request.query.keywords;
      return blogService.find().then((dataList) => {
        let options = {
          shouldSort: true,
          threshold: 0.3,
          location: 0,
          distance: 100,
          maxPatternLength: 32,
          minMatchCharLength: 3,
          keys: ['tags', 'title','category']
        };
        let fuse = new Fuse(dataList, options);
        return fuse.search(keywords);
      });
    }
  });
};
