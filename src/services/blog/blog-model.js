'use strict';

const Sequelize = require('sequelize');
module.exports = function (app) {
  return app.connection.define('blog', {
      id: {
        type: Sequelize.STRING,
        primaryKey: true,
        unique: true
      },
      createdBy: {
        field: "created_by",
        type: Sequelize.STRING
      },
      createdAt: {
        field: "created_on",
        type: Sequelize.DATE
      },
      updatedAt: {
        field: "updated_on",
        type: Sequelize.DATE
      },
      title: {
        type: Sequelize.STRING
      },
      shortDescription: {
        field: "short_description",
        type: Sequelize.STRING
      },
      type: {
        type: Sequelize.STRING
      },
      category: {
        type: Sequelize.STRING
      },
      thumbnail: {
        type: Sequelize.STRING
      },
      content: {
        type: Sequelize.STRING
      },
      comment: {
        type: Sequelize.JSON
      },
      show: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      tags: {
        type: Sequelize.JSON
      }
    },
    {
      tableName: 'blog'
    });
};
