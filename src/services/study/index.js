'use strict';

const service = require('feathers-sequelize');
const study = require('./study-model');
const hooks = require('./hooks');
const _ = require("lodash");
const Fuse = require("fuse.js");

module.exports = function() {
  const app = this;

  const options = {
    Model: study(app),
    paginate: {
    }
  };

  // Initialize our service with any options it requires
  app.use('/edu-pool-server/v1/study', service(options));

  // Get our initialize service to that we can bind hooks
  const studyService = app.service('/edu-pool-server/v1/study');

  // Set up our before hooks
  studyService.before(hooks.before);

  // Set up our after hooks
  studyService.after(hooks.after);

  //Search in study
  app.use('/edu-pool-server/v1/search/study', {
    find(request) {
      let keywords = request.query.keywords;
      return studyService.find().then((dataList) => {
        let options = {
          shouldSort: true,
          threshold: 0.3,
          location: 0,
          distance: 100,
          maxPatternLength: 32,
          minMatchCharLength: 3,
          keys: ['tags', 'title']
        };
        let fuse = new Fuse(dataList, options);
        return fuse.search(keywords);
      });
    }
  });
};
