'use strict';

const globalHooks = require('../../../hooks');
const hooks = require('feathers-hooks');
const prepareId = require("../../../utils").prepareId;

exports.before = {
  all: [],
  find: [],
  get: [],
  create: [
    function (hook, cb) {
      return prepareId(hook, 'blog', hook.data.title).then(function (id) {
        hook.data.id = id;
        cb();
      });
    }
  ],
  update: [],
  patch: [],
  remove: []
};

exports.after = {
  all: [],
  find: [],
  get: [],
  create: [],
  update: [],
  patch: [],
  remove: []
};
