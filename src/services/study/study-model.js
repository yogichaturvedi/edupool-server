'use strict';

const Sequelize = require('sequelize');
module.exports = function (app) {
  return app.connection.define('study', {
      id: {
        type: Sequelize.STRING,
        primaryKey: true,
        unique: true
      },
      createdBy: {
        field: "created_by",
        type: Sequelize.STRING
      },
      createdAt: {
        field: "created_on",
        type: Sequelize.DATE
      },
      updatedAt: {
        field: "updated_on",
        type: Sequelize.DATE
      },
      title: {
        type: Sequelize.STRING
      },
      description: {
        type: Sequelize.STRING
      },
      links: {
        type: Sequelize.JSON
      },
      tags: {
        type: Sequelize.JSON
      }
    },
    {
      tableName: 'study'
    });
};
