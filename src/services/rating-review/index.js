'use strict';

const service = require('feathers-sequelize');
const ratingAndReview = require('./rating-reivew');
const hooks = require('./hooks');
const _ = require("lodash");
const Fuse = require("fuse.js");

module.exports = function () {
  const app = this;

  const options = {
    Model: ratingAndReview(app),
    paginate: {}
  };

  // Initialize our service with any options it requires
  app.use('/edu-pool-server/v1/rating', service(options));

  // Get our initialize service to that we can bind hooks
  const ratingAndReviewService = app.service('/edu-pool-server/v1/rating');

  // Set up our before hooks
  ratingAndReviewService.before(hooks.before);

  // Set up our after hooks
  ratingAndReviewService.after(hooks.after);

  //Search in ratingAndReview
  app.use('/edu-pool-server/v1/search/rating', {
    find(request) {
      return ratingAndReviewService.find().then((dataList) => {
        let query = {
          instituteId: request.query.instituteId,
          instituteType: request.query.instituteType,
        };
      });
    }
  });

  app.use('/edu-pool-server/v1/search/rating', {
    create(request) {
      let query = {
        instituteId: request.query.instituteId,
        instituteType: request.query.instituteType,
      };
      return ratingAndReview(app).all({
        where: query
      });
    }
  });
};
