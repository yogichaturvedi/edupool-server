'use strict';

const Sequelize = require('sequelize');
module.exports = function (app) {
  return app.connection.define('rating-review', {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        unique: true,
        isUUID: 4,
        defaultValue: Sequelize.UUIDV4
      },
      createdBy: {
        field: "created_by",
        type: Sequelize.STRING,
        defaultValue: "Edupool Admin"
      },
      createdAt: {
        field: "created_on",
        type: Sequelize.DATE
      },
      updatedAt: {
        field: "updated_on",
        type: Sequelize.DATE
      },
      instituteId: {
        field: "institute_id",
        type: Sequelize.STRING
      },
      instituteType: {
        field: "institute_type",
        type: Sequelize.STRING
      },
      value: {
        type: Sequelize.INTEGER
      },
      user: {
        type: Sequelize.JSON
      },
      message: {
        type: Sequelize.STRING
      }
    },
    {
      tableName: 'rating-review'
    });
};
