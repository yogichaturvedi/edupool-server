'use strict';


const basic = require('./basic');
const coaching = require('./coaching');
const college = require('./college');
const school = require('./school');
const result = require('./result');
const event = require('./event');
const exam = require('./exam');
const study = require('./study');
const homeInfo = require('./home-info');
const user = require('./user');
const blog = require('./blog');

module.exports = function() {
  const app = this;
  app.configure(basic);
  app.configure(coaching);
  app.configure(college);
  app.configure(school);
  app.configure(result);
  app.configure(event);
  app.configure(exam);
  app.configure(study);
  app.configure(homeInfo);
  app.configure(user);
  app.configure(blog);
};
