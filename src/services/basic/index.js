'use strict';
const _ = require("lodash");

module.exports = function () {
  const app = this;

  app.use('/edu-pool-server/v1/basic-details', {
    find(request) {
      const promises = [];
      _.each(['school', 'college', 'coaching', 'event', 'exam', 'result', 'blog-article', 'blog-news'], (key) => {
        let query = {
          query: {
            $limit: 4,
            $sort: {
              createdAt: -1
            }
          }
        };

        if (key === 'school' || key === 'college' || key === 'coaching') {
          query.query.reviewed = true;
          query.query.featured = true;
        }

        if (key === 'blog-article') {
          query.query.show = true;
          query.query.type = 'article';
        }

        if (key === 'blog-news') {
          query.query.show = true;
          query.query.type = 'news';
          query.query.$limit = 10;
        }


        let uri = '/edu-pool-server/v1/' + (key === "blog-article" || key === "blog-news" ? "blog" : key);
        promises.push(app.service(uri)
          .find(query)
          .then(list => {
            return list
              .map((item) => {
                let response = {
                  id: item.id,
                  title: key === "blog-article" || key === "blog-news" ? item.title : item.basicDetails.title,
                  description: key === "blog-article" || key === "blog-news" ? item.shortDescription : item.basicDetails.description,
                  imageUrl: key === "blog-article" || key === "blog-news" ? item.thumbnail : item.basicDetails.imageUrl,
                  createdAt: item.createdAt,
                  createdBy: item.createdBy
                };
                if (key === 'school' || key === 'college' || key === 'coaching') {
                  response.address = {
                    city: item.basicDetails.address.city,
                    state: item.basicDetails.address.state
                  };
                }
                if (key === 'event') {
                  response.address = {
                    city: item.basicDetails.venue.city,
                    state: item.basicDetails.venue.state
                  };
                }
                return response;
              }) || []
          }));
      });

      return Promise.all(promises)
        .then(data => {
          return {
            school: data[0],
            college: data[1],
            coaching: data[2],
            event: data[3],
            exam: data[4],
            result: data[5],
            article: data[6],
            news: data[7]
          }
        });
    }
  });
};
