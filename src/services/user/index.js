'use strict';
const _ = require("lodash");

const service = require('feathers-sequelize');
const userModal = require('./user-model');
const hooks = require('./hooks');

module.exports = function () {
  const app = this;

  const options = {
    Model: userModal(app),
    paginate: {}
  };

  // Initialize our service with any options it requires
  app.use('/edu-pool-server/v1/user', service(options));

  // Get our initialize service to that we can bind hooks
  const userService = app.service('/edu-pool-server/v1/user');

  // Set up our before hooks
  userService.before(hooks.before);

  // Set up our after hooks
  userService.after(hooks.after);

  //Verify User
  app.use('/edu-pool-server/v1/verify-user', {
    create(request, body,res) {
      let query = {};
      if (request.email) {
        query.email = request.email;
      }
      if (request.password) {
        query.password = request.password;
      }
      return userModal(app).all({
        where: query
      }).then((matchedUser) => {

        if (matchedUser[0] && matchedUser[0].id) {
         return matchedUser;
        }
        return {
          status: "Unauthorized access",
          code: 401,
          message: "User Mismatch"
        }
      })
    }
  });

};
