'use strict';

const Sequelize = require('sequelize');
module.exports = function (app) {
  return app.connection.define('user', {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        unique: true,
        isUUID: 4,
        defaultValue: Sequelize.UUIDV4
      },
      createdBy: {
        field: "created_by",
        type: Sequelize.STRING
      },
      createdAt: {
        field: "created_on",
        type: Sequelize.DATE
      },
      updatedAt: {
        field: "updated_on",
        type: Sequelize.DATE
      },
      username: {
        field: "username",
        type: Sequelize.STRING
      },
      email: {
        field: "email",
        type: Sequelize.STRING
      },
      password: {
        field: "password",
        type: Sequelize.STRING
      },
      profilePhoto: {
        field: "profile_photo",
        type: Sequelize.STRING
      },
      mobileNumber: {
        field: "mobile_number",
        type: Sequelize.STRING
      },
    },
    {
      tableName: 'user'
    }
  );
};
