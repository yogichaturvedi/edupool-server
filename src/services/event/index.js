'use strict';

const service = require('feathers-sequelize');
const event = require('./event-model');
const hooks = require('./hooks');
const _ = require("lodash");
const Fuse = require("fuse.js");

module.exports = function() {
  const app = this;

  const options = {
    Model: event(app),
    paginate: {
      max: 25
    }
  };

  // Initialize our service with any options it requires
  app.use('/edu-pool-server/v1/event', service(options));

  // Get our initialize service to that we can bind hooks
  const eventService = app.service('/edu-pool-server/v1/event');

  // Set up our before hooks
  eventService.before(hooks.before);

  // Set up our after hooks
  eventService.after(hooks.after);

  //Search in event
  app.use('/edu-pool-server/v1/search/event', {
    find(request) {
      let keywords = request.query.keywords;
      return eventService.find().then((dataList) => {
        let options = {
          shouldSort: true,
          threshold: 0.3,
          location: 0,
          distance: 100,
          maxPatternLength: 32,
          minMatchCharLength: 3,
          keys: ['tags', 'basicDetails.title']
        };
        let fuse = new Fuse(dataList, options);
        return fuse.search(keywords);
      });
    }
  });
};
