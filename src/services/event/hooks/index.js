'use strict';

const hooks = require('feathers-hooks');
const _ = require('lodash');
const prepareId = require("../../../utils").prepareId;

exports.before = {
  all: [],
  find: [],
  get: [],
  create: [
    function (hook, cb) {
      return prepareId(hook, 'event', hook.data.basicDetails.title).then(function (id) {
        hook.data.id = id;
        cb();
      });
    }
  ],
  update: [],
  patch: [],
  remove: []
};

exports.after = {
  all: [],
  find: [],
  get: [
    //Append the similar school from same state and city
    function (hook) {
      return hook.app.service('/edu-pool-server/v1/event').find().then(events => {
        let similarCategory = _.filter(events, ((response) => {
          return (response.id !== hook.result.id) && response.basicDetails.category && (response.basicDetails.category === hook.result.basicDetails.category);
        }))
          .splice(0, 6)
          .map((item) => {
            return {
              id: item.id,
              basicDetails: item.basicDetails,
              tags: item.tags
            }
          });

        let similarState = (hook.result.basicDetails.venue && hook.result.basicDetails.venue.state) ? _.filter(events, ((response) => {
          return (response.id !== hook.result.id) && (response.basicDetails.venue && response.basicDetails.venue.state
            && (response.basicDetails.venue.state === hook.result.basicDetails.venue.state));
        }))
          .splice(0, 6)
          .map((item) => {
            return {
              id: item.id,
              basicDetails: item.basicDetails,
              tags: item.tags
            }
          }) : [];

        let similarCity = (hook.result.basicDetails.venue && hook.result.basicDetails.venue.city) ? _.filter(events, ((response) => {
          return (response.id !== hook.result.id) && (response.basicDetails.venue && response.basicDetails.venue.city
            && (response.basicDetails.venue.city === hook.result.basicDetails.venue.city));
        }))
          .splice(0, 6)
          .map((item) => {
            return {
              id: item.id,
              basicDetails: item.basicDetails,
              tags: item.tags
            }
          }) : [];


        hook.result.similar = {
          category: similarCategory,
          state: similarState,
          city: similarCity
        };
        return hook;
      });
    }
  ],
  create: [],
  update: [],
  patch: [],
  remove: []
};
