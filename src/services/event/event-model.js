'use strict';

const Sequelize = require('sequelize');
module.exports = function (app) {
  return app.connection.define('event', {
      id: {
        type: Sequelize.STRING,
        primaryKey: true,
        unique: true
      },
      createdBy: {
        field: "created_by",
        type: Sequelize.STRING
      },
      createdAt: {
        field: "created_on",
        type: Sequelize.DATE
      },
      updatedAt: {
        field: "updated_on",
        type: Sequelize.DATE
      },
      basicDetails: {
        field: "basic_details",
        type: Sequelize.JSON
      },
      activities: {
        type: Sequelize.JSON
      },
      tags: {
        type: Sequelize.JSON
      },
      faqs: {
        type: Sequelize.JSON
      },
      coverImage: {
        field: "cover_image",
        type: Sequelize.STRING
      },
      termsAndConditions: {
        field: "terms_and_conditions",
        type: Sequelize.JSON
      },
      socialLinks: {
        field: "social_links",
        type: Sequelize.JSON
      }
    },
    {
      tableName: 'event'
    });
};
