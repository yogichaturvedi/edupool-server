'use strict';

const service = require('feathers-sequelize');
const exam = require('./exam-model');
const hooks = require('./hooks');
const _ = require("lodash");
const Fuse = require("fuse.js");

module.exports = function() {
  const app = this;

  const options = {
    Model: exam(app),
    paginate: {
    }
  };

  // Initialize our service with any options it requires
  app.use('/edu-pool-server/v1/exam', service(options));

  // Get our initialize service to that we can bind hooks
  const examService = app.service('/edu-pool-server/v1/exam');

  // Set up our before hooks
  examService.before(hooks.before);

  // Set up our after hooks
  examService.after(hooks.after);

  //Search in exam
  app.use('/edu-pool-server/v1/search/exam', {
    find(request) {
      let keywords = request.query.keywords;
      return examService.find().then((dataList) => {
        let options = {
          shouldSort: true,
          threshold: 0.3,
          location: 0,
          distance: 100,
          maxPatternLength: 32,
          minMatchCharLength: 3,
          keys: ['tags', 'basicDetails.title']
        };
        let fuse = new Fuse(dataList, options);
        return fuse.search(keywords);
      });
    }
  });
};
