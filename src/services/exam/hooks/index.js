'use strict';

const globalHooks = require('../../../hooks');
const hooks = require('feathers-hooks');
const _ = require("lodash");
const prepareId = require("../../../utils").prepareId;

exports.before = {
  all: [],
  find: [],
  get: [],
  create: [
    function (hook, cb) {
      return prepareId(hook, 'exam', hook.data.basicDetails.title).then(function (id) {
        hook.data.id = id;
        cb();
      });
    }
  ],
  update: [],
  patch: [],
  remove: []
};

exports.after = {
  all: [],
  find: [],
  get: [
    //Append the similar organizer
    function (hook) {
      return hook.app.service('/edu-pool-server/v1/exam').find().then(exams => {
        let similarOrganizer = hook.result.basicDetails.organizer ? _.filter(exams, ((exam) => {
          return (exam.id !== hook.result.id) && exam.basicDetails.organizer && (exam.basicDetails.organizer
            && exam.basicDetails.organizer === hook.result.basicDetails.organizer);
        }))
          .splice(0, 6)
          .map((item) => {
            return {
              id: item.id,
              title: item.basicDetails.title,
              imageUrl: item.basicDetails.imageUrl
            }
          }) : [];
        hook.result.latest = _.reverse(_.sortBy(exams, ['createdAt'])).splice(0, 6)
          .map((item) => {
            return {
              id: item.id,
              title: item.basicDetails.title,
              imageUrl: item.basicDetails.imageUrl
            }
          }) || [];
        hook.result.similar = {
          organizer: similarOrganizer
        };
        return hook;
      });
    }
  ],
  create: [],
  update: [],
  patch: [],
  remove: []
};
