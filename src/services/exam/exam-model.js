'use strict';

const Sequelize = require('sequelize');
module.exports = function (app) {
  return app.connection.define('exam', {
      id: {
        type: Sequelize.STRING,
        primaryKey: true,
        unique: true
      },
      createdBy: {
        field: "created_by",
        type: Sequelize.STRING
      },
      createdAt: {
        field: "created_on",
        type: Sequelize.DATE
      },
      updatedAt: {
        field: "updated_on",
        type: Sequelize.DATE
      },
      basicDetails: {
        field: "basic_details",
        type: Sequelize.JSON
      },
      notice: {
        field: "notice",
        type: Sequelize.JSON
      },
      numberOfVacancies: {
        field: "number_of_vacancies",
        type: Sequelize.STRING
      },
      criteria: {
        field: "criteria",
        type: Sequelize.JSON
      },
      fees: {
        field: "fees",
        type: Sequelize.STRING
      },
      selectionProcedure: {
        field: "selection_procedure",
        type: Sequelize.STRING
      },
      importantDates: {
        field: "important_dates",
        type: Sequelize.JSON
      },
      howToApply: {
        field: "how_to_apply",
        type: Sequelize.JSON
      },
      officialWebsite: {
        field: "official_website",
        type: Sequelize.JSON
      },
      tags: {
        type: Sequelize.JSON
      }
    },
    {
      tableName: 'exam'
    });
};
