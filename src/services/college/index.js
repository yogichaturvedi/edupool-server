'use strict';

const service = require('feathers-sequelize');
const college = require('./college-model');
const hooks = require('./hooks');
const _ = require("lodash");
const Fuse = require("fuse.js");

module.exports = function () {
  const app = this;

  const options = {
    Model: college(app),
    paginate: {}
  };

  // Initialize our service with any options it requires
  app.use('/edu-pool-server/v1/college', service(options));

  // Get our initialize service to that we can bind hooks
  const collegeService = app.service('/edu-pool-server/v1/college');

  // Set up our before hooks
  collegeService.before(hooks.before);

  // Set up our after hooks
  collegeService.after(hooks.after);

  //Search in college
  app.use('/edu-pool-server/v1/search/college', {
    find(request) {
      let keywords = request.query.keywords;
      return collegeService.find().then((dataList) => {
        let options = {
          shouldSort: true,
          threshold: 0.3,
          location: 0,
          distance: 100,
          maxPatternLength: 32,
          minMatchCharLength: 3,
          keys: ['tags', 'basicDetails.title']
        };
        let fuse = new Fuse(dataList, options);
        return fuse.search(keywords);
      });
    }
  });
};
