'use strict';

const globalHooks = require('../../../hooks');
const hooks = require('feathers-hooks');
const _ = require('lodash');
const prepareId = require("../../../utils").prepareId;

exports.before = {
  all: [],
  find: [],
  get: [],
  create: [
    function (hook, cb) {
      return prepareId(hook, 'college', hook.data.basicDetails.title).then(function (id) {
        hook.data.id = id;
        cb();
      });
    }
  ],
  update: [],
  patch: [],
  remove: []
};

exports.after = {
  all: [],
  find: [],
  get: [
    //Append the similar colleges from same state and city
    function (hook) {
      return hook.app.service('/edu-pool-server/v1/college').find().then(colleges => {
        let similarState = (hook.result.basicDetails.address && hook.result.basicDetails.address.state) ? _.filter(colleges, ((response) => {
          return (response.id !== hook.result.id) && (response.basicDetails.address && response.basicDetails.address.state
            && (response.basicDetails.address.state === hook.result.basicDetails.address.state));
        }))
          .splice(0, 6)
          .map((item) => {
            return {
              id: item.id,
              title: item.basicDetails.title,
              imageUrl: item.basicDetails.imageUrl
            }
          }) : [];

        let similarCity = (hook.result.basicDetails.address && hook.result.basicDetails.address.city) ? _.filter(colleges, ((response) => {
          return (response.id !== hook.result.id) && (response.basicDetails.address && response.basicDetails.address.city
            && (response.basicDetails.address.city === hook.result.basicDetails.address.city));
        }))
          .splice(0, 6)
          .map((item) => {
            return {
              id: item.id,
              title: item.basicDetails.title,
              imageUrl: item.basicDetails.imageUrl
            }
          }) : [];


        hook.result.similar = {
          state: similarState,
          city: similarCity
        };
        return hook;
      });
    }
  ],
  create: [],
  update: [],
  patch: [],
  remove: []
};
