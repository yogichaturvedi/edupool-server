'use strict';

const Sequelize = require('sequelize');
module.exports = function (app) {
  return app.connection.define('result', {
      id: {
        type: Sequelize.STRING,
        primaryKey: true,
        unique: true
      },
      createdBy: {
        field: "created_by",
        type: Sequelize.STRING,
        defaultValue: "Edupool Admin"
      },
      createdAt: {
        field: "created_on",
        type: Sequelize.DATE
      },
      updatedAt: {
        field: "updated_on",
        type: Sequelize.DATE
      },
      basicDetails: {
        field:"basic_details",
        type: Sequelize.JSON
      },
      tags:{
        type:Sequelize.JSON
      }
    },
    {
      tableName: 'result'
    });
};
