'use strict';

const service = require('feathers-sequelize');
const result = require('./result-model');
const hooks = require('./hooks');
const _ = require("lodash");
const Fuse = require("fuse.js");

module.exports = function() {
  const app = this;

  const options = {
    Model: result(app),
    paginate: {

    }
  };

  // Initialize our service with any options it requires
  app.use('/edu-pool-server/v1/result', service(options));

  // Get our initialize service to that we can bind hooks
  const resultService = app.service('/edu-pool-server/v1/result');

  // Set up our before hooks
  resultService.before(hooks.before);

  // Set up our after hooks
  resultService.after(hooks.after);

  //Search in result
  app.use('/edu-pool-server/v1/search/result', {
    find(request) {
      let keywords = request.query.keywords;
      return resultService.find().then((dataList) => {
        let options = {
          shouldSort: true,
          threshold: 0.3,
          location: 0,
          distance: 100,
          maxPatternLength: 32,
          minMatchCharLength: 3,
          keys: ['tags','basicDetails.title']
        };
        let fuse = new Fuse(dataList, options);
        return fuse.search(keywords);
      });
    }
  });
};

/*
  app.use('/edu-pool-server/v1/search/result', {
    find(request) {
      let keywords = request.query.keywords;
      return resultService.find().then((dataList) => {
        return _.filter(dataList, (data) => {
          return _.some(data.tags, (tag) => {
            return _.includes(tag, keywords) || _.includes(keywords, tag);
          })
        })
      });
    }
  });
};*/
