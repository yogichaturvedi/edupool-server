'use strict';
const _ = require("lodash");
const globalHooks = require('../../../hooks');
const hooks = require('feathers-hooks');
const prepareId = require("../../../utils").prepareId;


exports.before = {
  all: [],
  find: [],
  get: [],
  create: [
    function (hook, cb) {
      return prepareId(hook, 'result', hook.data.basicDetails.title).then(function (id) {
        hook.data.id = id;
        cb();
      });
    }
  ],
  update: [],
  patch: [],
  remove: []
};

exports.after = {
  all: [],
  find: [],
  get: [
    //Append the latest results from similar board or university
    function (hook) {
      return hook.app.service('/edu-pool-server/v1/result').find().then(results => {
        let latestResults = hook.result.basicDetails.boardOrUniversity ? _.filter(results, ((response) => {
          return (response.id !== hook.result.id) && (response.basicDetails.boardOrUniversity
            && response.basicDetails.boardOrUniversity === hook.result.basicDetails.boardOrUniversity);
        }))
          .splice(0, 6)
          .map((item) => {
            return {
              id: item.id,
              title: item.basicDetails.title
            }
          }) : [];
        hook.result.similar = {
          boardOrUniversity: latestResults
        };
        return hook;
      });
    }
  ],
  create: [],
  update: [],
  patch: [],
  remove: []
};
