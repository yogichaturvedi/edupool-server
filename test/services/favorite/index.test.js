'use strict';

const assert = require('assert');
const app = require('../../../src/app');

describe('favorite service', function() {
  it('registered the favorites service', () => {
    assert.ok(app.service('favorites'));
  });
});
