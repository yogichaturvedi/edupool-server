'use strict';

const assert = require('assert');
const app = require('../../../src/app');

describe('rx-order service', function() {
  it('registered the rxOrders service', () => {
    assert.ok(app.service('rxOrders'));
  });
});
